/*
 * Задача 1. Вторая статистика (RMQ)
 * Дано число N и последовательность из N целых чисел. Найти вторую порядковую статистику на заданных диапазонах.
 * Для решения задачи используйте структуру данных Sparse Table. Требуемое время обработки каждого диапазона O(1).
 * Время подготовки структуры данных O(NlogN). Дополнительная память - O(NlogN)
*/
#include <cmath>
#include <iostream>
#include <set>
#include <vector>

using std::cin;
using std::cout;
using std::pair;
using std::set;
using std::vector;

class SparseTable {
  struct data {
    int first_min_index;
    int second_min_index;
    int first_min;
    int second_min;
  };
  data compare(const data &d1, const data &d2);
  data getMin(int left, int right);
  vector<vector<data>> table;

 public:
  SparseTable(const vector<int> &array);
  int getFirstMin(int left, int right);
  int getSecondMin(int left, int right);
};

int main() {
  int numberCount, queriesCount;

  cin >> numberCount >> queriesCount;
  vector<int> numbers(numberCount);
  for (auto &it : numbers) 
    cin >> it;

  SparseTable table(numbers);
  for (int i = 0; i < queriesCount; i++) {
    int left, right;
    cin >> left >> right;
    left--; // Нумерация с 0
    right--;
    cout << table.getSecondMin(left, right) << std::endl;
  }
}

SparseTable::SparseTable(const vector<int> &array) {
  int size = array.size();
  int maxj = log2(size) + 1;
  table.resize(size + 1, vector<data>(maxj));
  for (int i = size - 1; i >= 0; i--)
    for (int j = 0; i + (1 << j) <= size; j++)
      if (j == 0)
        table[i][j] = {i, -1, array[i], INT32_MAX}; // Второй минимум - INF
      else
        table[i][j] =
            compare(table[i][j - 1], table[i + (1 << (j - 1))][j - 1]);
}

SparseTable::data SparseTable::getMin(int left, int right) {
  int logLen = log2(right - left + 1);
  return compare(table[left][logLen], table[right - (1 << logLen) + 1][logLen]);
}

int SparseTable::getFirstMin(int left, int right) {
  return getMin(left, right).first_min;
}

int SparseTable::getSecondMin(int left, int right) {
  return getMin(left, right).second_min;
}

SparseTable::data SparseTable::compare(const SparseTable::data &d1,
                                       const SparseTable::data &d2) {
  set<pair<int, int>> temp;
  temp.insert({d1.first_min, d1.first_min_index}); // Отсортируем пары (min, index) и выберем 2 минимума
  temp.insert({d2.first_min, d2.first_min_index});
  temp.insert({d1.second_min, d1.second_min_index});
  temp.insert({d2.second_min, d2.second_min_index});
  auto beg = temp.begin();
  pair<int, int> firstMin = *beg;
  beg++;
  pair<int, int> secondMin = *beg;
  data answer = {firstMin.second, secondMin.second, firstMin.first, secondMin.first};
  return answer;
}
